﻿namespace WindowsFormsApp7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTry = new System.Windows.Forms.Label();
            this.txtB_Slovo = new System.Windows.Forms.TextBox();
            this.ButtonNIgra = new System.Windows.Forms.Button();
            this.labelSlovo = new System.Windows.Forms.Label();
            this.buttonEnter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelSlova = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelTry
            // 
            this.labelTry.AutoSize = true;
            this.labelTry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTry.Location = new System.Drawing.Point(82, 153);
            this.labelTry.Name = "labelTry";
            this.labelTry.Size = new System.Drawing.Size(13, 13);
            this.labelTry.TabIndex = 0;
            this.labelTry.Text = "6";
            // 
            // txtB_Slovo
            // 
            this.txtB_Slovo.Location = new System.Drawing.Point(151, 84);
            this.txtB_Slovo.Name = "txtB_Slovo";
            this.txtB_Slovo.Size = new System.Drawing.Size(100, 20);
            this.txtB_Slovo.TabIndex = 1;
            // 
            // ButtonNIgra
            // 
            this.ButtonNIgra.Location = new System.Drawing.Point(15, 235);
            this.ButtonNIgra.Name = "ButtonNIgra";
            this.ButtonNIgra.Size = new System.Drawing.Size(100, 23);
            this.ButtonNIgra.TabIndex = 2;
            this.ButtonNIgra.Text = "Nova igra";
            this.ButtonNIgra.UseVisualStyleBackColor = true;
            this.ButtonNIgra.Click += new System.EventHandler(this.btn_newWord_Click);
            // 
            // labelSlovo
            // 
            this.labelSlovo.AutoSize = true;
            this.labelSlovo.Location = new System.Drawing.Point(148, 59);
            this.labelSlovo.Name = "labelSlovo";
            this.labelSlovo.Size = new System.Drawing.Size(34, 13);
            this.labelSlovo.TabIndex = 3;
            this.labelSlovo.Text = "Slovo";
            // 
            // buttonEnter
            // 
            this.buttonEnter.Location = new System.Drawing.Point(268, 81);
            this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(75, 23);
            this.buttonEnter.TabIndex = 4;
            this.buttonEnter.Text = "Enter";
            this.buttonEnter.UseVisualStyleBackColor = true;
            this.buttonEnter.Click += new System.EventHandler(this.btn_enter_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Pokušaji:";
            // 
            // LabelSlova
            // 
            this.LabelSlova.AutoSize = true;
            this.LabelSlova.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelSlova.Location = new System.Drawing.Point(150, 188);
            this.LabelSlova.Name = "LabelSlova";
            this.LabelSlova.Size = new System.Drawing.Size(13, 13);
            this.LabelSlova.TabIndex = 6;
            this.LabelSlova.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 188);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Broj preostalih slova:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(327, 427);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Quit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 462);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LabelSlova);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonEnter);
            this.Controls.Add(this.labelSlovo);
            this.Controls.Add(this.ButtonNIgra);
            this.Controls.Add(this.txtB_Slovo);
            this.Controls.Add(this.labelTry);
            this.Name = "Form1";
            this.Text = "Vješala";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTry;
        private System.Windows.Forms.TextBox txtB_Slovo;
        private System.Windows.Forms.Button ButtonNIgra;
        private System.Windows.Forms.Label labelSlovo;
        private System.Windows.Forms.Button buttonEnter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabelSlova;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}

