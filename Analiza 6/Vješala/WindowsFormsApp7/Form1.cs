﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {
        List<string> wordList = new List<string>();
        string path = "words.txt";
        string wordOfChoice;
        int attemptsLeft;
        int lettersLeft;
        string used;

        public Form1()
        {
            InitializeComponent();
        }

        public int countLettersInChoice(string let) {
            int count = 0;
            int i;
            for(i = 0; i < wordOfChoice.Length; i++) {
                if (wordOfChoice[i].ToString() == let) {
                    count++;
                }
            }
            return count;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_enter_Click(object sender, EventArgs e)
        {
            string letter;
            letter = txtB_Slovo.Text;
            int occurances;

            if (letter.Length > 1 || letter == "" || letter.Contains(" "))
            {
                MessageBox.Show("Unos krivi", "Samo jedno slovo, bez razmaka!!");
            }
            else if (used.Contains(letter)) {
                MessageBox.Show("Vec iskoristeno slovo");
            }
            else
            {
                if ((occurances = countLettersInChoice(letter)) == 0)
                {
                    attemptsLeft--;
                    labelTry.Text = attemptsLeft.ToString();
                }
                else
                {
                    lettersLeft -= occurances;
                    used += letter;
                    LabelSlova.Text = lettersLeft.ToString();
                }
            }

            if (attemptsLeft <= 0)
            {
                labelTry.Text = "IZGUBLJENO";
                LabelSlova.Text = "IZGUBLJENO";
                buttonEnter.Enabled = false;
            }
            else if (lettersLeft <= 0)
            {
                labelTry.Text = "POBJEDA!!";
                LabelSlova.Text = "POBJEDA!!";
                buttonEnter.Enabled = false;
            }
        }

        private void btn_newWord_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int index = random.Next(wordList.Count());
            wordOfChoice = wordList[index];

            lettersLeft = wordOfChoice.Length;
            attemptsLeft = 6;
            used = "";
            LabelSlova.Text = (lettersLeft).ToString();
            labelTry.Text = attemptsLeft.ToString();
            buttonEnter.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string word;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path)) {
                while ((word = reader.ReadLine()) != null) {
                    wordList.Add(word);
                }
            }

            Random random = new Random();
            int index = random.Next(wordList.Count());
            wordOfChoice = wordList[index];

            lettersLeft = wordOfChoice.Length;
            attemptsLeft = 6;
            used = "";
            LabelSlova.Text = (lettersLeft).ToString();
            labelTry.Text = attemptsLeft.ToString();
            
        }
    }
}
