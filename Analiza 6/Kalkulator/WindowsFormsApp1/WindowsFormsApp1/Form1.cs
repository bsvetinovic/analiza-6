﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private double op1, op2;
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1))
            {
                MessageBox.Show("Wrong input of operand 1");
            }
            else if (!double.TryParse(textBox2.Text, out op2))
            {
                MessageBox.Show("Wrong input of perand 2");
            }

            label1.Text = (op1 - op2).ToString();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1))
            {
                MessageBox.Show("Wrong input of operand 1");
            }
            else if (!double.TryParse(textBox2.Text, out op2))
            {
                MessageBox.Show("Wrong input of perand 2");
            }

            label1.Text = (op1 * op2).ToString();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1))
            {
                MessageBox.Show("Wrong input of operand 1");
            }
            else if (!double.TryParse(textBox2.Text, out op2))
            {
                MessageBox.Show("Wrong input of operand 2");
            }

            if(op2 == 0)
            {
                MessageBox.Show("op2 cant be 0!");
            }

            label1.Text = (op1 / op2).ToString();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1))
            {
                MessageBox.Show("Wrong input of operand 1");
            }

            label1.Text = Math.Sin(op1).ToString();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1))
            {
                MessageBox.Show("Wrong input of operand 1");
            }

            label1.Text = Math.Cos(op1).ToString();

        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1))
            {
                MessageBox.Show("Wrong input of operand 1");
            }

            if (op1 <= 0)
            {
                MessageBox.Show("op must be > 0 !!");
            }

            label1.Text = Math.Log10(op1).ToString();

        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1))
            {
                MessageBox.Show("Wrong input of operand 1");
            }
            if(op1 < 0)
            {
                MessageBox.Show("op must be >= 0 !!");
            }
            label1.Text = Math.Sqrt(op1).ToString();

        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1))
            {
                MessageBox.Show("Wrong input of operand 1");
            }

            label1.Text = Math.Floor(op1).ToString();

        }

        private void button10_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out op1))
            {
                MessageBox.Show("Wrong input of operand 1");
            }
            else if (!double.TryParse(textBox2.Text, out op2))
            {
                MessageBox.Show("Wrong input of operand 2");
            }

            label1.Text = (op1 + op2).ToString();

        }

    }
}
